
#! /bin/bash

apt-get update 
apt-get install -y mariadb-client
apt-get install -y apache2 php
apt-get install -y php-mysql

apt-get install -y wget
cd /usr/local/bin
sudo wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
gsutil cp -r gs://m-are-team-bucket/m-are-team-34a92bb9d5ce.json .

sudo chmod +x cloud_sql_proxy
cd /etc/systemd/system/
gsutil cp -r gs://m-are-team-bucket/cloud-sql-proxy.service .

sudo systemctl daemon-reload
sudo systemctl start cloud-sql-proxy

cd /var/www/html
rm index.html -f
rm index.php -f
wget https://storage.googleapis.com/cloud-training/gcpnet/httplb/index.php
META_REGION_STRING=$(curl "http://metadata.google.internal/computeMetadata/v1/instance/zone" -H "Metadata-Flavor: Google")
REGION=`echo "$META_REGION_STRING" | awk -F/ '{print $4}'`
sed -i "s|region-here|$REGION|" index.php

gsutil cp -r gs://m-are-team-bucket/script_php/* .
sudo systemctl restart apache2
