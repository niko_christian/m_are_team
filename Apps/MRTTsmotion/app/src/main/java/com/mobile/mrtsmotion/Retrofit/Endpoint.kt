package com.mobile.mrtsmotion.Retrofit

import com.mobile.mrtsmotion.MainModel
import retrofit2.Call
import retrofit2.http.GET

interface Endpoint {
    @GET("data.php")
    fun data(): Call<MainModel>
}