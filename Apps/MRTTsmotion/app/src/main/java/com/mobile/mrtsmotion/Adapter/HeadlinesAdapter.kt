package com.mobile.mrtsmotion.Adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.mrtsmotion.MainActivity
import com.mobile.mrtsmotion.Notification.ProcessFragment
import com.mobile.mrtsmotion.R
import com.mobile.mrtsmotion.Tes
import kotlinx.android.synthetic.main.violation_item.view.*

class HeadlinesAdapter(private val newsList: List<Tes>,
                       private val context: Context) : RecyclerView.Adapter<HeadlinesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.violation_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = newsList[position]

        holder.tempat.text = note.tempat_pelanggaran
        holder.tanggal.tanggal_item_home.text = note.tanggal
        holder.waktu.text = note.waktu
        Glide.with(holder.itemView)
            .load("http://storage.googleapis.com${note.gambar}")
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .centerCrop()
            .into(holder.gambar)


            holder.itemView.bt_next.setOnClickListener {
                note.let {
                    val bundle = Bundle()
                    bundle.putParcelable("NOTE",it)
                    val processFragment = ProcessFragment()
                    processFragment.arguments = bundle
                    (context as MainActivity).supportFragmentManager.beginTransaction().replace(R.id.fragment_blank1, processFragment).commit()

                }
            }

    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    inner class ViewHolder(view: View, var note:Tes?=null) : RecyclerView.ViewHolder(view) {
        val tempat: TextView = view.findViewById(R.id.judul_item_home)
        val tanggal: TextView = view.findViewById(R.id.tanggal_item_home)
        val waktu : TextView = view.findViewById(R.id.waktu_item_home)
        val gambar : ImageView =  view.findViewById(R.id.img_item_home)


    }

}




