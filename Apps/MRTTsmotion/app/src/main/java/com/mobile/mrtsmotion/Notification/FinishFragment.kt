package com.mobile.mrtsmotion.Notification

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobile.mrtsmotion.Adapter.FinishAdapter
import com.mobile.mrtsmotion.Adapter.HeadlinesAdapter
import com.mobile.mrtsmotion.MainModel
import com.mobile.mrtsmotion.R
import com.mobile.mrtsmotion.Retrofit.Service
import com.mobile.mrtsmotion.Tes
import kotlinx.android.synthetic.main.fragment_finish.*
import kotlinx.android.synthetic.main.fragment_waiting.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FinishFragment : Fragment() {
    private var bundle: Bundle? = null
    private var tes: Tes? = null
    private lateinit var mAdapter: FinishAdapter
    private val TAG: String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getDataFromApi()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_finish, container, false)
        return view
    }
    private fun getDataFromApi(){
        Service.endpoint.data()
            .enqueue(object : Callback<MainModel> {
                override fun onFailure(call: Call<MainModel>, t: Throwable) {
                    printLog( t.toString() )
                }
                override fun onResponse(
                    call: Call<MainModel>,
                    response: Response<MainModel>
                ) {
                    if (response.isSuccessful) {
                        val newsList = response.body()!!.data
                        mAdapter = FinishAdapter(newsList, context!!)
                        val mLayoutManager = LinearLayoutManager(context)
                        rv_finish.layoutManager = mLayoutManager
                        rv_finish.adapter = mAdapter
                    }
                }
            })
    }
    private fun printLog(message: String) {
        Log.d(TAG, message)
    }


}