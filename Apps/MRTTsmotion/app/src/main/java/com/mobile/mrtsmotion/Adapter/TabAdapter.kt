package com.mobile.mrtsmotion.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mobile.mrtsmotion.Notification.BlankFinishFragment
import com.mobile.mrtsmotion.Notification.BlankProcessFragment
import com.mobile.mrtsmotion.Notification.FinishFragment
import com.mobile.mrtsmotion.Notification.WaitingFragment

class TabAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager)  {
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> WaitingFragment()
            1 -> BlankProcessFragment()
            else -> BlankFinishFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when(position){
            0 -> "Waiting"
            1 -> "Process"
            else -> "Finish"
        }
    }
}