package com.mobile.mrtsmotion.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobile.mrtsmotion.R
import com.mobile.mrtsmotion.Tes

class FinishAdapter(private val newsList: List<Tes>,
                       private val context: Context
) : RecyclerView.Adapter<FinishAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.finish_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = newsList[position]

        holder.tanggal.text = note.tanggal
        holder.waktu.text = note.waktu
        Glide.with(holder.itemView)
            .load("http://storage.googleapis.com${note.gambar}")
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .centerCrop()
            .into(holder.gambar)

    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    inner class ViewHolder(view: View, var note: Tes?=null) : RecyclerView.ViewHolder(view) {
        val tanggal: TextView = view.findViewById(R.id.date_finish)
        val waktu : TextView = view.findViewById(R.id.time_finish)
        val gambar : ImageView =  view.findViewById(R.id.img_finish)


    }

}