package com.mobile.mrtsmotion.Notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.mobile.mrtsmotion.MainActivity
import com.mobile.mrtsmotion.R
import com.mobile.mrtsmotion.Tes
import kotlinx.android.synthetic.main.fragment_process.*


class ProcessFragment : Fragment() {
    private lateinit var location: TextView
    private lateinit var gambar: ImageView
    private lateinit var tanggal: TextView
    private lateinit var waktu: TextView
    private lateinit var name: TextInputEditText
    private lateinit var handphone: TextInputEditText
    private lateinit var adress: TextInputEditText


    private var bundle: Bundle? = null
    private var tes: Tes? = null
    var finish1: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bundle = this.arguments
        if (bundle != null){
            tes = bundle!!.getParcelable("NOTE")
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_process, container, false)
        location = view.findViewById(R.id.location)
        tanggal = view.findViewById(R.id.tanggal)
        waktu = view.findViewById(R.id.waktu)
        gambar = view.findViewById(R.id.img)
        name = view.findViewById(R.id.inputEditText1)
        handphone = view.findViewById(R.id.inputEditText2)
        adress = view.findViewById(R.id.inputEditText3)
        finish1 = view.findViewById(R.id.button_finish) as Button


        name.setText(tes?.nama_lengkap)
        handphone.setText(tes?.no_hp)
        adress.setText(tes?.alamat)

        finish1?.setOnClickListener {
            tes.let {
                val bundle = Bundle()
                bundle.putParcelable("FINISH",it)
                val processFragment = FinishFragment()
                processFragment.arguments = bundle
                (context as MainActivity).supportFragmentManager.beginTransaction().replace(R.id.fragment_blank2, processFragment).commit()
            }
        }

        setData()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    private fun setData() {
        location.text = tes?.tempat_pelanggaran
        tanggal.text = tes?.tanggal
        waktu.text = tes?.waktu
        Glide.with(this)
            .load("http://storage.googleapis.com${tes?.gambar}" )
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .into(gambar)

    }

}