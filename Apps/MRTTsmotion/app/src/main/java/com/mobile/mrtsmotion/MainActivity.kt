package com.mobile.mrtsmotion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mobile.mrtsmotion.Notification.PlaceholderFragment
import com.mobile.mrtsmotion.Notification.WaitingFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()
        val bottonNav = findViewById<BottomNavigationView>(R.id.botton_navigation)
        replaceFragment(PlaceholderFragment())
        bottonNav.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.menu_notification->{
                    replaceFragment(PlaceholderFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                else -> false
            }
        }
    }
    private fun replaceFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}