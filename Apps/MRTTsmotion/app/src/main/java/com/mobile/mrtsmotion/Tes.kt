package com.mobile.mrtsmotion

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tes (
    val pelanggaranID: Int,
    val tempat_pelanggaran: String,
    val tanggal: String,
    val waktu: String,
    val gambar: String,
    val status: Int,
    val nama_lengkap: String,
    val no_hp:String,
    val alamat:String
): Parcelable
